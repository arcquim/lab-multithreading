Условие задания [здесь](https://docs.google.com/document/d/1qb5B0Tw7UrmgVpx_WFbF54c6Kl8dTeWY9pY4UFshgdc/edit)

#################################################

В директории лежат входные текстовые файлы, проименованные следующим образом: in_<N>.dat, где N - натуральное число.

Каждый файл состоит из двух строк. В первой строке - число, обозначающее действие, а во второй - числа с плавающей точкой, разделенные пробелом.

Действия могут быть следующими: 
1 - сложение
2 - умножение
3 - сумма квадратов

Необходимо написать многопоточное приложение, которое выполнит требуемые действия над числами и сумму результатов запишет в файл out.dat. Название рабочей директории передается в виде аргумента рабочей строки.

В реализации приветствуется использование полиморфизма и паттернов проектирования.

-------------------------------------------------------

As long as we do not need do use any of additional third-party libraries, except JDK - we do not use any kind of build tool, like Ant, Maven or Gradle

So - no JUnit test out-of-the-box