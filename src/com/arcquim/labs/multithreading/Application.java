package com.arcquim.labs.multithreading;

import com.arcquim.labs.multithreading.datasource.IdentifiersDataSource;
import com.arcquim.labs.multithreading.environment.EnvironmentContainer;
import com.arcquim.labs.multithreading.environment.EnvironmentResolver;
import com.arcquim.labs.multithreading.postprocessor.ResultPostprocessor;
import com.arcquim.labs.multithreading.processor.IdentifiersProcessor;
import static com.arcquim.labs.multithreading.utils.Constants.OUTPUT_FILE_NAME;

import java.util.List;

/**
 * Created by Aleksandr Erin on 25.11.2017.
 */
public class Application {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        String baseDirectory = getBaseDirectoryFromArgs(args);
        Application application = new Application(baseDirectory);
        application.start();
    }

    private static String getBaseDirectoryFromArgs(String[] args) throws InstantiationException, IllegalAccessException {
        BeansFactory beansFactory = BeansFactory.getInstance();
        EnvironmentContainer environmentContainer = beansFactory.getEnvironmentContainer();
        environmentContainer.setCommandLineArguments(args);
        EnvironmentResolver environmentResolver = beansFactory.getEnvironmentResolver();
        return environmentResolver.resolveBaseDirectoryName(environmentContainer);
    }

    private String baseDirectory;

    public Application(String baseDirectory) {
        if (baseDirectory == null || baseDirectory.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.baseDirectory = baseDirectory;
    }

    public void start() throws InstantiationException, IllegalAccessException {
        BeansFactory beansFactory = BeansFactory.getInstance();
        IdentifiersDataSource identifiersDataSource = beansFactory.getIdentifiersDataSource();
        List<String> identifiers = identifiersDataSource.getIdentifiers(this.baseDirectory);
        IdentifiersProcessor identifiersProcessor = beansFactory.getIdentifiersProcessor();
        String result = identifiersProcessor.processIdentifiers(identifiers);
        ResultPostprocessor resultPostprocessor = beansFactory.getResultPostprocessor();
        resultPostprocessor.processResult(result, this.baseDirectory + '/' + OUTPUT_FILE_NAME);
    }

}
