package com.arcquim.labs.multithreading.utils;

/**
 * Created by Aleksandr Erin on 25.11.2017.
 */
public final class Constants {

    public static final String INSTANTIATION_DISABLED_EXCEPTION_TEXT = "This class is not designed to be instantiated";
    public static final String METHOD_NOT_SUPPORTED_EXCEPTION_TEXT = "This method is not implemented yet";
    public static final String CONTEXT_CANNOT_BE_NULL_EXCEPTION_TEXT = "Context cannot be null";
    public static final String DIRECTORY_NAME_CANNOT_BE_BLANK_EXCEPTION_TEXT = "Directory name cannot be blank";
    public static final String DIRECTORY_DOES_NOT_EXIST_EXCEPTION_TEXT = "The following directory does not exist: ";
    public static final String IDENTIFIERS_EMPTY_EXCEPTION_TEXT = "Identifiers cannot be empty";
    public static final String INVALID_BINARY_OPERATION_EXCEPTION_TEXT = "Invalid binary operation: ";
    public static final String INVALID_UNARY_OPERATION_EXCEPTION_TEXT = "Invalid unary operation: ";
    public static final String BLANK_BINARY_OPERATION_EXCEPTION_TEXT = "Blank binary operation or empty binary executor are not allowed";
    public static final String BLANK_UNARY_OPERATION_EXCEPTION_TEXT = "Blank unary operation or empty unary executor are not allowed";
    public static final String COULD_NOT_WRITE_TO_FILE_EXCEPTION_TEXT = "Could not write to file: ";

    public static final String INPUT_FILE_TEMPLATE = "in_\\d+\\.dat";
    public static final String OUTPUT_FILE_NAME = "out.dat";

    public static final String DIRECTORY_NAME_KEY = "baseDir";
    public static final String DEFAULT_DIRECTORY_NAME = "resources/test";

    public static final int DEFAULT_NUMBER_OF_THREADS = 5;

    private Constants() {
        throw new IllegalStateException(INSTANTIATION_DISABLED_EXCEPTION_TEXT);
    }

}
