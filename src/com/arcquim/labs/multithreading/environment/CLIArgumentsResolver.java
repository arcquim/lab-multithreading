package com.arcquim.labs.multithreading.environment;

import java.util.Arrays;

import static com.arcquim.labs.multithreading.utils.Constants.DEFAULT_DIRECTORY_NAME;
import static com.arcquim.labs.multithreading.utils.Constants.DIRECTORY_NAME_KEY;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public class CLIArgumentsResolver implements EnvironmentResolver {

    @Override
    public String resolveBaseDirectoryName(EnvironmentContainer environmentContainer) {
        if (environmentContainer == null) {
            return DEFAULT_DIRECTORY_NAME;
        }
        String[] cliArgs = environmentContainer.getCommandLineArguments();
        if (cliArgs == null || cliArgs.length == 0) {
            return DEFAULT_DIRECTORY_NAME;
        }
        int argumentPosition = Arrays.asList(cliArgs).indexOf(DIRECTORY_NAME_KEY);
        if (argumentPosition < 0 || argumentPosition == cliArgs.length - 1) {
            return DEFAULT_DIRECTORY_NAME;
        }
        return cliArgs[argumentPosition + 1];
    }
}
