package com.arcquim.labs.multithreading.environment;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public interface EnvironmentContainer {

    String[] getCommandLineArguments();
    void setCommandLineArguments(String[] args);

    String[] getOpenshiftContainerVariables();
    void setOpenshiftContainerVariables(String[] vars);

}
