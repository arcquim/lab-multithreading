package com.arcquim.labs.multithreading.environment;

import static com.arcquim.labs.multithreading.utils.Constants.METHOD_NOT_SUPPORTED_EXCEPTION_TEXT;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public class EnvironmentContainerDefaultImpl implements EnvironmentContainer {

    private String[] commandLineArguments;

    @Override
    public String[] getCommandLineArguments() {
        return commandLineArguments;
    }

    @Override
    public void setCommandLineArguments(String[] args) {
        this.commandLineArguments = args;
    }

    @Override
    public String[] getOpenshiftContainerVariables() {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_EXCEPTION_TEXT);
    }

    @Override
    public void setOpenshiftContainerVariables(String[] vars) {
        throw new UnsupportedOperationException(METHOD_NOT_SUPPORTED_EXCEPTION_TEXT);
    }
}
