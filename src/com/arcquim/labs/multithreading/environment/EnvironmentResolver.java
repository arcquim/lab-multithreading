package com.arcquim.labs.multithreading.environment;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public interface EnvironmentResolver {

    String resolveBaseDirectoryName(EnvironmentContainer environmentContainer);

}
