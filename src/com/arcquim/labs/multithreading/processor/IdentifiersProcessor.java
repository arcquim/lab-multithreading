package com.arcquim.labs.multithreading.processor;

import java.util.List;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public interface IdentifiersProcessor {

    String processIdentifiers(List<String> identifiers);
    List<String> getAllIdentifiers();
    void submitPartialResult(double partialResult);

}
