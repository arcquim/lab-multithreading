package com.arcquim.labs.multithreading.processor;

import java.util.ArrayList;
import java.util.List;

import static com.arcquim.labs.multithreading.utils.Constants.DEFAULT_NUMBER_OF_THREADS;
import static com.arcquim.labs.multithreading.utils.Constants.IDENTIFIERS_EMPTY_EXCEPTION_TEXT;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public class DefaultIdentifiersProcessor implements IdentifiersProcessor {

    private List<String> allIdentifiers;
    private double result;

    @Override
    public String processIdentifiers(List<String> identifiers) {
        if (identifiers == null || identifiers.isEmpty()) {
            throw new IllegalArgumentException(IDENTIFIERS_EMPTY_EXCEPTION_TEXT);
        }
        allIdentifiers = identifiers;
        result = 0.;
        int identifiersNumber = identifiers.size();
        int threadsNumber = getNumberOfThread();
        int identifiersPerThread = identifiersNumber / threadsNumber;
        int numberOfUndistributedIdentifiers = identifiersNumber - identifiersPerThread * threadsNumber;
        int threadStart = 0;
        List<Thread> threads = new ArrayList<>();
        for (int threadIndex = 0; threadIndex < threadsNumber; threadIndex++) {
            if (threadStart >= identifiersNumber) {
                break;
            }
            int threadEnd = Math.min(
                    threadStart + identifiersPerThread + (numberOfUndistributedIdentifiers-- > 0 ? 1 : 0),
                    identifiersNumber);
            threads.add(new Thread(new ComputingRunnableTask(threadStart, threadEnd, this)));
            threadStart = threadEnd;
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return Double.toString(result);
    }

    @Override
    public List<String> getAllIdentifiers() {
        return allIdentifiers;
    }

    @Override
    public synchronized void submitPartialResult(double partialResult) {
        result += partialResult;
    }

    protected int getNumberOfThread() {
        return DEFAULT_NUMBER_OF_THREADS;
    }

}
