package com.arcquim.labs.multithreading.processor;

import com.arcquim.labs.multithreading.solver.Solver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Aleksandr Erin on 28.11.2017.
 */
public class ComputingRunnableTask implements Runnable {

    private int bunchStart;
    private int bunchEnd;
    private IdentifiersProcessor identifiersProcessor;
    private Solver solver;
    private double partialResult;

    public ComputingRunnableTask(int bunchStart, int bunchEnd, IdentifiersProcessor identifiersProcessor) {
        this.bunchStart = bunchStart;
        this.bunchEnd = bunchEnd;
        this.identifiersProcessor = identifiersProcessor;
        this.solver = new Solver();
    }

    @Override
    public void run() {
        List<String> allIdentifiers = identifiersProcessor.getAllIdentifiers();
        int identifiersNumber = allIdentifiers.size();
        partialResult = 0;
        for (int i = bunchStart; i < bunchEnd; i++) {
            if (i >= identifiersNumber) {
                break;
            }
            Path path = Paths.get(allIdentifiers.get(i));
            try {
                List<String> fileLines = Files.lines(path).collect(Collectors.toList());
                if (fileLines.size() >= 2) {
                    String operation = fileLines.get(0);
                    String[] operands = fileLines.get(1).split(" ");
                    Double leftOperand = null, rightOperand = null;
                    for (String operand : operands) {
                        if (!operand.trim().isEmpty()) {
                            try {
                                if (leftOperand == null) {
                                    leftOperand = Double.parseDouble(operand);
                                } else if (rightOperand == null) {
                                    rightOperand = Double.parseDouble(operand);
                                } else {
                                    break;
                                }
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (leftOperand != null && rightOperand != null) {
                        partialResult += solver.executeBinaryOperation(operation, leftOperand, rightOperand);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        identifiersProcessor.submitPartialResult(partialResult);
    }
}
