package com.arcquim.labs.multithreading.datasource;

import java.util.List;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public interface IdentifiersDataSource {

    List<String> getIdentifiers(String sourceLocation);

}
