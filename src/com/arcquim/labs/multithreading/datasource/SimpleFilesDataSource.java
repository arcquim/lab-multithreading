package com.arcquim.labs.multithreading.datasource;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import static com.arcquim.labs.multithreading.utils.Constants.INPUT_FILE_TEMPLATE;
import static com.arcquim.labs.multithreading.utils.Constants.DIRECTORY_NAME_CANNOT_BE_BLANK_EXCEPTION_TEXT;
import static com.arcquim.labs.multithreading.utils.Constants.DIRECTORY_DOES_NOT_EXIST_EXCEPTION_TEXT;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public class SimpleFilesDataSource implements IdentifiersDataSource {

    @Override
    public List<String> getIdentifiers(String baseDirectoryPath) {
        if (baseDirectoryPath == null || baseDirectoryPath.trim().isEmpty()) {
            throw new IllegalArgumentException(DIRECTORY_NAME_CANNOT_BE_BLANK_EXCEPTION_TEXT);
        }
        File baseDirectory = new File(baseDirectoryPath);
        if (!baseDirectory.exists() || !baseDirectory.isDirectory()) {
            throw new IllegalArgumentException(DIRECTORY_DOES_NOT_EXIST_EXCEPTION_TEXT + baseDirectoryPath);
        }
        List<String> result = new ArrayList<>();
        File[] files = baseDirectory.listFiles();
        if (files == null || files.length == 0) {
            return result;
        }
        for (File file : files) {
            if (file.isFile() && file.getName().matches(INPUT_FILE_TEMPLATE)) {
                result.add(file.getAbsolutePath());
            }
        }
        return result;
    }
}
