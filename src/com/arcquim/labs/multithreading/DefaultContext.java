package com.arcquim.labs.multithreading;

import com.arcquim.labs.multithreading.datasource.IdentifiersDataSource;
import com.arcquim.labs.multithreading.datasource.SimpleFilesDataSource;
import com.arcquim.labs.multithreading.environment.CLIArgumentsResolver;
import com.arcquim.labs.multithreading.environment.EnvironmentContainer;
import com.arcquim.labs.multithreading.environment.EnvironmentContainerDefaultImpl;
import com.arcquim.labs.multithreading.environment.EnvironmentResolver;
import com.arcquim.labs.multithreading.postprocessor.ResultPostprocessor;
import com.arcquim.labs.multithreading.postprocessor.SimpleFileResultPostprocessor;
import com.arcquim.labs.multithreading.processor.DefaultIdentifiersProcessor;
import com.arcquim.labs.multithreading.processor.IdentifiersProcessor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public class DefaultContext implements Context {

    private final Map<Class<?>, Class<?>> IMPLEMENTATIONS_MAP = new HashMap<>();;

    public DefaultContext() {
        addImplementation(EnvironmentContainer.class, EnvironmentContainerDefaultImpl.class);
        addImplementation(EnvironmentResolver.class, CLIArgumentsResolver.class);
        addImplementation(IdentifiersDataSource.class, SimpleFilesDataSource.class);
        addImplementation(IdentifiersProcessor.class, DefaultIdentifiersProcessor.class);
        addImplementation(ResultPostprocessor.class, SimpleFileResultPostprocessor.class);
    }

    @Override
    public <T> Class<? extends T> getBeanImplementationClass(Class<T> api) {
        return (Class<? extends T>) IMPLEMENTATIONS_MAP.get(api);
    }

    @Override
    public <T> void addImplementation(Class<T> api, Class<? extends T> implementation) {
        if (api != null && implementation != null && api.isAssignableFrom(implementation)) {
            IMPLEMENTATIONS_MAP.put(api, implementation);
        }
    }
}
