package com.arcquim.labs.multithreading.solver;

import java.util.HashMap;
import java.util.Map;

import static com.arcquim.labs.multithreading.utils.Constants.INVALID_BINARY_OPERATION_EXCEPTION_TEXT;
import static com.arcquim.labs.multithreading.utils.Constants.INVALID_UNARY_OPERATION_EXCEPTION_TEXT;
import static com.arcquim.labs.multithreading.utils.Constants.BLANK_BINARY_OPERATION_EXCEPTION_TEXT;
import static com.arcquim.labs.multithreading.utils.Constants.BLANK_UNARY_OPERATION_EXCEPTION_TEXT;

/**
 * Created by Aleksandr Erin on 02.12.2017.
 */
public class Solver {

    private final Map<String, BinaryOperationExecutor> binaryOperationExecutors;
    private final Map<String, UnaryOperationExecutor> unaryOperationExecutors;

    public Solver() {
        binaryOperationExecutors = new HashMap<>();
        binaryOperationExecutors.put("1", new AdditionExecutor());
        binaryOperationExecutors.put("2", new MultiplicationExecutor());
        binaryOperationExecutors.put("3", new DoubleLengthExecutor());
        unaryOperationExecutors = new HashMap<>();
    }

    public double executeBinaryOperation(String operation, double leftOperand, double rightOperand) {
        BinaryOperationExecutor binaryOperationExecutor = binaryOperationExecutors.get(operation);
        if (binaryOperationExecutor == null) {
            throw new IllegalArgumentException(INVALID_BINARY_OPERATION_EXCEPTION_TEXT + operation);
        }
        return binaryOperationExecutor.execute(leftOperand, rightOperand);
    }

    public double executeUnaryOperation(String operation, double operand) {
        UnaryOperationExecutor unaryOperationExecutor = unaryOperationExecutors.get(operation);
        if (unaryOperationExecutor == null) {
            throw new IllegalArgumentException(INVALID_UNARY_OPERATION_EXCEPTION_TEXT + operation);
        }
        return unaryOperationExecutor.executeOperation(operand);
    }

    public void addBinaryOperationExecutor(String operation, BinaryOperationExecutor binaryOperationExecutor) {
        if (operation == null || operation.trim().isEmpty() || binaryOperationExecutor == null) {
            throw new IllegalArgumentException(BLANK_BINARY_OPERATION_EXCEPTION_TEXT);
        }
        binaryOperationExecutors.put(operation, binaryOperationExecutor);
    }

    public void addUnaryOperationExecutor(String operation, UnaryOperationExecutor unaryOperationExecutor) {
        if (operation == null || operation.trim().isEmpty() || unaryOperationExecutor == null) {
            throw new IllegalArgumentException(BLANK_UNARY_OPERATION_EXCEPTION_TEXT);
        }
        unaryOperationExecutors.put(operation, unaryOperationExecutor);
    }

}
