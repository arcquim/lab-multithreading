package com.arcquim.labs.multithreading.solver;

/**
 * Created by Aleksandr Erin on 02.12.2017.
 */
public interface BinaryOperationExecutor {

    double execute(double leftOperand, double rightOperand);

}
