package com.arcquim.labs.multithreading.solver;

/**
 * Created by Aleksandr Erin on 03.12.2017.
 */
public interface UnaryOperationExecutor {

    double executeOperation(double operand);

}
