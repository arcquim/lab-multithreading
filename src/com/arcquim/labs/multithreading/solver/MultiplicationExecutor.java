package com.arcquim.labs.multithreading.solver;

/**
 * Created by Aleksandr Erin on 03.12.2017.
 */
public class MultiplicationExecutor implements BinaryOperationExecutor {

    @Override
    public double execute(double leftOperand, double rightOperand) {
        return leftOperand * rightOperand;
    }
}
