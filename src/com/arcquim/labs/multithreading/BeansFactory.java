package com.arcquim.labs.multithreading;

import com.arcquim.labs.multithreading.datasource.IdentifiersDataSource;
import com.arcquim.labs.multithreading.environment.EnvironmentContainer;
import com.arcquim.labs.multithreading.environment.EnvironmentResolver;
import com.arcquim.labs.multithreading.postprocessor.ResultPostprocessor;
import com.arcquim.labs.multithreading.processor.IdentifiersProcessor;

import static com.arcquim.labs.multithreading.utils.Constants.CONTEXT_CANNOT_BE_NULL_EXCEPTION_TEXT;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public class BeansFactory {

    public static BeansFactory getInstance(Context context) {
        return new BeansFactory(context);
    }

    public static BeansFactory getInstance() {
        return getInstance(new DefaultContext());
    }

    private final Context context;

    private BeansFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException(CONTEXT_CANNOT_BE_NULL_EXCEPTION_TEXT);
        }
        this.context = context;
    }

    public EnvironmentContainer getEnvironmentContainer() throws IllegalAccessException, InstantiationException {
        Class<? extends EnvironmentContainer> environmentContainerClass =
                context.getBeanImplementationClass(EnvironmentContainer.class);
        return environmentContainerClass.newInstance();
    }

    public EnvironmentResolver getEnvironmentResolver() throws IllegalAccessException, InstantiationException {
        Class<? extends EnvironmentResolver> environmentResolverClass =
                context.getBeanImplementationClass(EnvironmentResolver.class);
        return environmentResolverClass.newInstance();
    }

    public IdentifiersDataSource getIdentifiersDataSource() throws IllegalAccessException, InstantiationException {
        Class<? extends IdentifiersDataSource> identifiersDataSourceClass =
                context.getBeanImplementationClass(IdentifiersDataSource.class);
        return identifiersDataSourceClass.newInstance();
    }

    public IdentifiersProcessor getIdentifiersProcessor() throws IllegalAccessException, InstantiationException {
        Class<? extends IdentifiersProcessor> identifiersProcessorClass =
                context.getBeanImplementationClass(IdentifiersProcessor.class);
        return identifiersProcessorClass.newInstance();
    }

    public ResultPostprocessor getResultPostprocessor() throws IllegalAccessException, InstantiationException {
        Class<? extends ResultPostprocessor> resultPostprocessorClass =
                context.getBeanImplementationClass(ResultPostprocessor.class);
        return resultPostprocessorClass.newInstance();
    }

}
