package com.arcquim.labs.multithreading.postprocessor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import static com.arcquim.labs.multithreading.utils.Constants.COULD_NOT_WRITE_TO_FILE_EXCEPTION_TEXT;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public class SimpleFileResultPostprocessor implements ResultPostprocessor {

    @Override
    public void processResult(String result, String resultLocation) {
        try {
            Files.write(Paths.get(resultLocation), result.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(COULD_NOT_WRITE_TO_FILE_EXCEPTION_TEXT + resultLocation);
        }
    }
}
