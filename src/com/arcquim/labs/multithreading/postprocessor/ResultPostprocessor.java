package com.arcquim.labs.multithreading.postprocessor;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public interface ResultPostprocessor {

    void processResult(String result, String resultLocation);

}
