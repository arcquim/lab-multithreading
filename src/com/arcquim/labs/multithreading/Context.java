package com.arcquim.labs.multithreading;

/**
 * Created by Aleksandr Erin on 26.11.2017.
 */
public interface Context {

    <T> Class<? extends T> getBeanImplementationClass(Class<T> api);
    <T> void addImplementation(Class<T> api, Class<? extends T> implementation);

}
